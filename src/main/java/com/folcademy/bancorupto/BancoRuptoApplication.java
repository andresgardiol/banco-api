package com.folcademy.bancorupto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoRuptoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoRuptoApplication.class, args);
	}

}
