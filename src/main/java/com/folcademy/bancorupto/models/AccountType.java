package com.folcademy.bancorupto.models;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}
