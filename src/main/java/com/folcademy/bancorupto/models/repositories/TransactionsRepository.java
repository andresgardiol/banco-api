package com.folcademy.bancorupto.models.repositories;

import com.folcademy.bancorupto.models.entities.TransactionEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface TransactionsRepository extends CrudRepository<TransactionEntity, Long> {
    List<TransactionEntity> findAllByOriginOrDestination(String origin, String destination);
}
