package com.folcademy.bancorupto.models.repositories;

import com.folcademy.bancorupto.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<UserEntity, String> {

}
