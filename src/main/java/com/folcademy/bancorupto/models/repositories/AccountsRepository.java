package com.folcademy.bancorupto.models.repositories;


import com.folcademy.bancorupto.models.entities.AccountEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AccountsRepository extends CrudRepository<AccountEntity, Long> {

    List<AccountEntity> findAccountEntitiesByUserDni(String userDni);

}
