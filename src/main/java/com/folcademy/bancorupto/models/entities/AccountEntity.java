package com.folcademy.bancorupto.models.entities;

import com.folcademy.bancorupto.models.AccountType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity(name = "accounts")
public class AccountEntity {

    @Id
    private Long number;
    private String cbu;
    @Enumerated(EnumType.STRING)
    private AccountType type;
    private String userDni;

    public AccountEntity(Long number, String cbu, AccountType type, String userDni) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userDni = userDni;
    }

    public AccountEntity() {
    }

    public Long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountType getType() {
        return type;
    }

    public String getUserDni() {
        return userDni;
    }
}
