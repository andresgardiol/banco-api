package com.folcademy.bancorupto.models.entities;

import com.folcademy.bancorupto.models.Currency;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "transactions")
public class TransactionEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name="date", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date date;
    private String description;
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private String origin;
    private String destination;

    public TransactionEntity() {
    }

    public TransactionEntity(long id, Date date, String description, BigDecimal amount, Currency currency, String origin, String destination) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }
}
