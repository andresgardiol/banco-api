package com.folcademy.bancorupto.models;

public enum Currency {
    ARS, USD, EUR
}
