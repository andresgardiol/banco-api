package com.folcademy.bancorupto.controllers;

import com.folcademy.bancorupto.models.entities.TransactionEntity;
import com.folcademy.bancorupto.models.repositories.TransactionsRepository;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {

    private final TransactionsRepository transactionsRepository;

    public TransactionsController(TransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    @GetMapping("/{accountNumber}")
    public List<TransactionEntity> getTransactionsForAccount(@PathVariable String accountNumber) {
        return transactionsRepository.findAllByOriginOrDestination(accountNumber, accountNumber);
    }
}
