package com.folcademy.bancorupto.controllers;

import com.folcademy.bancorupto.models.entities.AccountEntity;
import com.folcademy.bancorupto.models.repositories.AccountsRepository;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountsRepository accountsRepository;

    public AccountsController(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @GetMapping("/user/{userDni}")
    public List<AccountEntity> getAccountsByUserDni(@PathVariable String userDni) {
        return accountsRepository.findAccountEntitiesByUserDni(userDni);
    }

    @PostMapping("/new")
    public AccountEntity createNewAccount(@RequestBody AccountEntity accountEntity) {
        return accountsRepository.save(accountEntity);
    }
}
