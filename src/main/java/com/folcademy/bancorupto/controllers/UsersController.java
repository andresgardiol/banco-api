package com.folcademy.bancorupto.controllers;

import com.folcademy.bancorupto.models.entities.UserEntity;
import com.folcademy.bancorupto.models.repositories.UsersRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersRepository usersRepository;

    public UsersController(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @GetMapping("/{id}")
    public UserEntity getUserById(@PathVariable String id) {
        return usersRepository.findById(id).get();
    }

    @PostMapping("/new")
    public UserEntity createNewUser(@RequestBody UserEntity userEntity) {
        return usersRepository.save(userEntity);
    }
}
